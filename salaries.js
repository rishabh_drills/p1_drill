// 4. Find the sum of all salaries

const inventory = require('./inventory');

// const convertArr = inventory.forEach((element) => { element['salary'] = parseFloat(element['salary'].replace("$", ""))})
function sumOfSalaries(inventory){
    const sum = inventory.reduce((accumulator, object) => {
    accumulator += parseFloat(object['salary'].replace("$", ""));
   return accumulator;
  }, 0);
  return sum;
}
const resultSum = sumOfSalaries(inventory); 
  console.log(resultSum);

//   146.29999999999998