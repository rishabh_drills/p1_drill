// 2.Convert all the salary values into proper numbers instead of strings 

const inventory = require('./inventory');

function convertSalary(inventory){
    let salary = [];
    let index = 0;
     inventory.forEach((element) => { element['salary'] = parseFloat(element['salary'].replace("$", ""));
        salary[index++] = element['salary'];
      
    });
       return salary;
}
const result = convertSalary(inventory);
console.log(result);