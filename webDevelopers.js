const inventory = require('./inventory');

function webDevelopers(inventory) {
    return inventory.filter(user => user["job"].includes("Web Developer"));

}
let result = webDevelopers(inventory);
console.log(result);

/*[
    {
      id: 1,
      first_name: 'Gregg',
      last_name: 'Lacey',
      job: 'Web Developer III',
      salary: '$3.62',
      location: 'Malta'
    },
    {
      id: 4,
      first_name: 'Sherwood',
      last_name: 'Adriano',
      job: 'Web Developer II',
      salary: '$6.46',
      location: 'Brazil'
    }
  ]*/