// 5. Find the sum of all salaries based on country using only HOF method.

const inventory = require('./inventory');

/*let countryCount = inventory.reduce(function(allLocations, location){
    if(!allLocations[location['location']]){
        allLocations[location['location']] = 1;
    }else{
        allLocations[location['location']]++;
    }
    return allLocations;
},{});
console.log(countryCount);

output => {
    Malta: 1,
    France: 1,
    'U.S. Virgin Islands': 1,
    Brazil: 2,
    'Costa Rica': 1,
    Colombia: 2,
    Poland: 2,
    China: 5,
    Russia: 2,
    'United States': 1,
    Indonesia: 3,
    Peru: 1,
    Macedonia: 1,
    Croatia: 1,
    Zambia: 1,
    'South Africa': 1,
    'South Korea': 1,
    Azerbaijan: 1,
    Thailand: 1,
    Chile: 1
  }
*/



let countryCount = inventory.reduce(function(allLocations, country){
    let value = parseFloat(country['salary'].replace("$", ""));
    if(!allLocations[country['location']]){
        allLocations[country['location']] = value;
    }else{
        allLocations[country['location']] += value;
    }
    return allLocations;
},{});
console.log(countryCount);

/*
output => salaries = {
    Malta: 3.62,
    France: 4.16,
    'U.S. Virgin Islands': 4.34,
    Brazil: 11.74,
    'Costa Rica': 6.4,
    Colombia: 9.059999999999999,
    Poland: 8.04,
    China: 22.54,
    Russia: 8.14,
    'United States': 9.28,
    Indonesia: 22.87,
    Peru: 3.63,
    Macedonia: 4.18,
    Croatia: 6.62,
    Zambia: 0.89,
    'South Africa': 0.49,
    'South Korea': 1.65,
    Azerbaijan: 9.42,
    Thailand: 6.62,
    Chile: 2.61
  }
  */