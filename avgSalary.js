// 6. Find the average salary of based on country using only HOF method

const inventory = require('./inventory');


let countryCount = inventory.reduce(function(allLocations, country){
    if(!allLocations[country['location']]){
        // console.log("Y")
        allLocations[country['location']] = parseFloat(country['salary'].replace("$", ""));
    }else{
        allLocations[country['location']] = (allLocations[country['location']]+parseFloat(country['salary'].replace("$", "")))/2;
        // console.log("n")
    }
    return allLocations;
},{});
console.log(countryCount);